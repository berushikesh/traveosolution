﻿using System;
using static TraveoSolution.View;
using static TraveoSolution.Controller.StudentEnrollmentController;

namespace TraveoSolution.ToDos
{
    public static class StudentEnrollmentToDo
    {
        public static char GoTo { get; set; }

        public static char Handle(int mc)
        {
            try
            {
                switch (mc)
                {
                    case 1:
                        getAllStudentEnrolled();
                        GoTo = alert() == 1 ? 'D' : ' ';
                        break;
                    case 2:
                        enrollStudent();
                        GoTo = alert() == 1 ? 'D' : ' ';
                        break;
                    case 3:
                        updateStudentEnrollment();
                        GoTo = alert() == 1 ? 'D' : ' ';
                        break;
                    case 4:
                        deleteEnrolledStudent();
                        GoTo = alert() == 1 ? 'D' : ' ';
                        break;
                    default:
                        invalidEntry();
                        GoTo = alert() == 1 ? 'D' : ' ';
                        break;
                }
            }
            catch (Exception e)
            {
                log(e);
            }

            return GoTo;
        }
    }
}