﻿using static TraveoSolution.View;

namespace TraveoSolution.ToDos
{
    public static class ToDo
    {
        public static char GoTo { get; set; }

        public static char Handle(int mm)
        {
            switch (mm)
            {
                case 1:
                    A:
                    divider();
                    mm = toDO();
                    if (StudentToDo.Handle(mm) == 'A')
                        goto A;
                    else
                        GoTo = toMenu() == 1 ? 'T' : ' ';
                    break;


                case 2:
                    B:
                    divider();
                    mm = toDO();
                    if (CourseToDo.Handle(mm) == 'B')
                        goto B;
                    else
                        GoTo = toMenu() == 1 ? 'T' : ' ';
                    break;


                case 3:
                    C:
                    divider();
                    mm = toDO();
                    if (ProfessorToDo.Handle(mm) == 'C')
                        goto C;
                    else
                        GoTo = toMenu() == 1 ? 'T' : ' ';
                    break;


                case 4:
                    D:
                    divider();
                    mm = toDO();
                    if (StudentEnrollmentToDo.Handle(mm) == 'D')
                        goto D;
                    else
                        GoTo = toMenu() == 1 ? 'T' : ' ';
                    break;

                default:
                    invalidEntry();
                    GoTo = toMenu() == 1 ? 'T' : ' ';
                    break;
            }

            return GoTo;
        }
    }
}