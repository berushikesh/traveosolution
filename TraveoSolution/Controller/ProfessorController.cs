﻿using System;
using System.Drawing;
using System.Linq;
using static TraveoSolution.View;
using Console = Colorful.Console;

namespace TraveoSolution.Controller
{
    public static class ProfessorController
    {
        private static readonly DBDataContext Context = new DBDataContext();

        public static void addProfessor()
        {
            divider();
            Console.Write("\t ENTER PROFESSOR NAME: ", Color.White);
            var p = new Professor
            {
                ProfessorName = Console.ReadLine()
            };
            Console.WriteLine("\n\t COURSE ID IS NEEDED. SELECT FROM FOLLOWING. ");
            CourseController.getAllCourses();
            Console.Write("\t ENTER COURSE ID: ", Color.White);
            var temp = Convert.ToInt32(Console.ReadLine());
            var course = Context.Courses.Select(e => e.CourseId);
            if (course.Contains(temp))
            {
                p.CourseId = temp;
                Context.Professors.InsertOnSubmit(p);
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD INSERTED ", Color.SpringGreen);
                getProfessor(p.ProfessorName, p.CourseId);
            }
            else
            {
                invalidEntry();
            }
        }

        public static void updateProfessor()
        {
            divider();

            Console.WriteLine("\n\t PROFESSOR NAME AND COURSE ID IS NEEDED. SELECT FROM FOLLOWING. ");
            getAllProfessors();

            Console.Write("\t ENTER INDEX: ", Color.White);
            var i = Convert.ToInt32(Console.ReadLine());

            var professor = Context.Professors.SingleOrDefault(e => e.Index == i);

            if (professor != null)
            {
                Console.Write("\t ENTER NEW PROFESSOR NAME: ", Color.White);
                professor.ProfessorName = Console.ReadLine();
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD UPDATED ", Color.SpringGreen);
                getProfessor(professor.ProfessorName, professor.CourseId);
            }
            else
            {
                invalidEntry();
            }
        }

        public static void getAllProfessors()
        {
            var professors = Context.Professors;
            header();
            foreach (var professor in professors)
                Console.WriteLine(
                    "\t INDEX: " + professor.Index + " | PROFESSOR NAME: " + professor.ProfessorName +
                    " | COURSE NAME: " + professor.Course.CourseName + " | COURSE ID: " + professor.CourseId,
                    Color.OrangeRed);

            divider();
        }

        public static void getProfessor(string pn, int cid)
        {
            var professor =
                Context.Professors.SingleOrDefault(e => e.ProfessorName == pn && Equals(e.CourseId, cid));
            header();

            if (professor != null)
                Console.WriteLine(
                    "\t PROFESSOR NAME: " + professor.ProfessorName + " | COURSE ID: " + professor.CourseId,
                    Color.OrangeRed);
            else
                invalidEntry();

            divider();
        }

        public static void deleteProfessor()
        {
            divider();

            Console.WriteLine("\n\t INDEX IS NEEDED. SELECT FROM FOLLOWING. ");
            getAllProfessors();

            Console.Write("\t ENTER INDEX: ", Color.White);
            var id = Convert.ToInt32(Console.ReadLine());
            var professor = Context.Professors.SingleOrDefault(e => e.Index == id);
            if (professor != null)
            {
                Context.Professors.DeleteOnSubmit(professor);
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD DELETED", Color.SpringGreen);
                getAllProfessors();
            }
            else
            {
                invalidEntry();
            }
        }
    }
}