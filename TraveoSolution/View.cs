﻿using System;
using System.Drawing;
using System.Reflection;
using log4net;
using Console = Colorful.Console;

namespace TraveoSolution
{
    public static class View
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void divider()
        {
            Console.WriteLine(
                "-----------------------------------------------------------------------------------------------------------------------",
                Color.DarkSlateGray);
        }

        public static void header()
        {
            divider();
            Console.WriteLine("\t RESULT", Color.Orchid);
        }

        public static int menu()
        {
            try
            {
                divider();
                Console.Write("\t 1: STUDENT \n\t 2: COURSE \n\t 3: PROFESSOR \n\t 4: STUDENT ENROLLMENT\n\n ENTER: ",
                    Color.DeepSkyBlue);
                return Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int toDO()
        {
            try
            {
                Console.Write("\t 1: GET\n\t 2: ADD\n\t 3: UPDATE\n\t 4: DELETE\n\n ENTER: ", Color.SkyBlue);
                return Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int toMenu()
        {
            try
            {
                divider();
                Console.WriteLine(" GO BACK TO THE MAIN MENU", Color.DarkOrange);
                divider();
                return alert();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int alert()
        {
            try
            {
                Console.Write(" CONFIRMATION\n\n WANT TO CONTINUE? 1: YES 2: NO\n\n ENTER: ", Color.Gold);
                return Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static void invalidEntry()
        {
            divider();
            Console.WriteLine(" Invalid Entry :( ", Color.IndianRed);
            divider();
        }

        public static void log(Exception e)
        {
            var log = e.StackTrace;
            divider();
            Logger.Info(log);
            Console.WriteLine("\t SOMETHING WENT WRONG. RESULT IS LOGGED", Color.Olive);
            divider();
        }
    }
}