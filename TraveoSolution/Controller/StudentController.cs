﻿using System;
using System.Drawing;
using System.Linq;
using static TraveoSolution.View;
using Console = Colorful.Console;

namespace TraveoSolution.Controller
{
    public static class StudentController
    {
        private static readonly DBDataContext Context = new DBDataContext();

        public static void addStudent()
        {
            divider();
            Console.Write("\t ENTER STUDENT NAME: ", Color.White);
            var st = new Student
            {
                StudentName = Console.ReadLine()
            };
            Context.Students.InsertOnSubmit(st);
            Context.SubmitChanges();
            divider();
            Console.WriteLine(" SUCCESS\n\n RECORD INSERTED ", Color.SpringGreen);
            getStudent(st.StudentName);
        }

        public static void updateStudent()
        {
            divider();
            Console.WriteLine("\n\t STUDENT ID IS NEEDED. SELECT FROM FOLLOWING. ", Color.White);
            getAllStudents();

            Console.Write("\t ENTER STUDENT ID: ", Color.White);
            var id = Convert.ToInt32(Console.ReadLine());
            var student = Context.Students.SingleOrDefault(e => e.StudentId == id);
            Console.Write("\t ENTER STUDENT NAME: ", Color.White);
            if (student != null)
            {
                student.StudentName = Console.ReadLine();
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD UPDATED ", Color.SpringGreen);
                getStudent(student.StudentName);
            }
            else
            {
                invalidEntry();
            }
        }

        public static void deleteStudent()
        {
            divider();

            Console.WriteLine("\n\t STUDENT ID IS NEEDED. SELECT FROM FOLLOWING. ", Color.White);
            getAllStudents();

            Console.Write("\t ENTER STUDENT ID: ", Color.White);
            var id = Convert.ToInt32(Console.ReadLine());
            var student = Context.Students.SingleOrDefault(e => e.StudentId == id);
            if (student != null)
            {
                Context.Students.DeleteOnSubmit(student);
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD DELETED ", Color.SpringGreen);
                getAllStudents();
            }
            else
            {
                invalidEntry();
            }
        }

        public static void getAllStudents()
        {
            var students = Context.Students;
            header();
            foreach (var student in students)
                Console.WriteLine("\t STUDENT ID: " + student.StudentId + " | STUDENT NAME: " + student.StudentName,
                    Color.OrangeRed);

            divider();
        }

        public static void getStudent(string stn)
        {
            var student = Context.Students.SingleOrDefault(e => e.StudentName == stn);
            header();

            if (student != null)
                Console.WriteLine("\t STUDENT ID: " + student.StudentId + " | STUDENT NAME: " + student.StudentName,
                    Color.OrangeRed);
            else
                invalidEntry();

            divider();
        }
    }
}