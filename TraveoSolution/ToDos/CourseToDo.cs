﻿using System;
using static TraveoSolution.View;
using static TraveoSolution.Controller.CourseController;

namespace TraveoSolution.ToDos
{
    public static class CourseToDo
    {
        public static char GoTo { get; set; }

        public static char Handle(int mc)
        {
            try
            {
                switch (mc)
                {
                    case 1:
                        getAllCourses();
                        GoTo = alert() == 1 ? 'B' : ' ';
                        break;
                    case 2:
                        addCourse();
                        GoTo = alert() == 1 ? 'B' : ' ';
                        break;
                    case 3:
                        updateCourse();
                        GoTo = alert() == 1 ? 'B' : ' ';
                        break;
                    case 4:
                        deleteCourse();
                        GoTo = alert() == 1 ? 'B' : ' ';
                        break;
                    default:
                        invalidEntry();
                        GoTo = alert() == 1 ? 'B' : ' ';
                        break;
                }
            }
            catch (Exception e)
            {
                log(e);
            }

            return GoTo;
        }
    }
}