﻿using System;
using System.Drawing;
using System.Linq;
using static TraveoSolution.View;
using Console = Colorful.Console;

namespace TraveoSolution.Controller
{
    public static class StudentEnrollmentController
    {
        private static readonly DBDataContext Context = new DBDataContext();

        public static void enrollStudent()
        {
            divider();

            Console.WriteLine("\n\t STUDENT ID AND COURSE ID IS NEEDED. SELECT FROM FOLLOWING. ");
            StudentController.getAllStudents();
            ProfessorController.getAllProfessors();

            var se = new StudentEnrollment();

            Console.Write("\t ENTER STUDENT ID: ", Color.White);
            var tSid = se.StudentId = Convert.ToInt32(Console.ReadLine());

            Console.Write("\t ENTER COURSE ID: ", Color.White);
            var tCid = Convert.ToInt32(Console.ReadLine());

            var student = Context.Students.Select(e => e.StudentId);
            var course = Context.Professors.Select(e => e.CourseId);

            if (student.Contains(tSid) && course.Contains(tCid))
            {
                se.StudentId = tSid;
                se.CourseId = tCid;
                Context.StudentEnrollments.InsertOnSubmit(se);
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD INSERTED ", Color.SpringGreen);
                getEnrolledStudent(se.StudentId, se.CourseId);
            }
            else
            {
                invalidEntry();
            }
        }

        public static void updateStudentEnrollment()
        {
            divider();
            Console.WriteLine("\t JUST DELETE PREVIOUS ONE AND ADD NEW! :)",Color.Olive);	            
            divider();
        }

        public static void getAllStudentEnrolled()
        {
            var studentEnrollment = Context.StudentEnrollments;

            header();

            foreach (var se in studentEnrollment.OrderBy(e => e.CourseId))
            {
                var p = Context.Professors.SingleOrDefault(r => r.CourseId == se.CourseId);
                if (p != null)
                    Console.WriteLine(
                        "\t INDEX: " + se.Index + " | STUDENT ID: " + se.StudentId + " | Student NAME: " +
                        se.Student.StudentName + " | COURSE ID: " + se.CourseId + " | COURSE NAME: " +
                        se.Course.CourseName + " | PROFESSOR NAME: " + p.ProfessorName,
                        Color.OrangeRed);
            }

            divider();
        }

        public static void getEnrolledStudent(int sid, int cid)
        {
            var se = Context.StudentEnrollments.SingleOrDefault(e => e.StudentId == sid && Equals(e.CourseId, cid));
            header();

            if (se != null)
                Console.WriteLine(
                    "\t INDEX: " + se.Index + " | STUDENT ID: " + se.StudentId + " | Student NAME: " +
                    se.Student.StudentName + " | COURSE ID: " + se.CourseId + " | COURSE NAME: " +
                    se.Course.CourseName,
                    Color.OrangeRed);
            else
                invalidEntry();

            divider();
        }

        public static void deleteEnrolledStudent()
        {
            divider();

            Console.WriteLine("\n\t INDEX IS NEEDED. SELECT FROM FOLLOWING. ");
            getAllStudentEnrolled();

            Console.Write("\t ENTER INDEX: ", Color.White);
            var id = Convert.ToInt32(Console.ReadLine());
            var se = Context.StudentEnrollments.SingleOrDefault(e => e.Index == id);
            if (se != null)
            {
                Context.StudentEnrollments.DeleteOnSubmit(se);
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD DELETED", Color.SpringGreen);
                getAllStudentEnrolled();
            }
            else
            {
                invalidEntry();
            }
        }
    }
}