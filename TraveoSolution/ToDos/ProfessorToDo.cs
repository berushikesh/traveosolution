﻿using System;
using static TraveoSolution.View;
using static TraveoSolution.Controller.ProfessorController;

namespace TraveoSolution.ToDos
{
    public static class ProfessorToDo
    {
        public static char GoTo { get; set; }

        public static char Handle(int mc)
        {
            try
            {
                switch (mc)
                {
                    case 1:
                        getAllProfessors();
                        GoTo = alert() == 1 ? 'C' : ' ';
                        break;
                    case 2:
                        addProfessor();
                        GoTo = alert() == 1 ? 'C' : ' ';
                        break;
                    case 3:
                        updateProfessor();
                        GoTo = alert() == 1 ? 'C' : ' ';
                        break;
                    case 4:
                        deleteProfessor();
                        GoTo = alert() == 1 ? 'C' : ' ';
                        break;
                    default:
                        invalidEntry();
                        GoTo = alert() == 1 ? 'C' : ' ';
                        break;
                }
            }
            catch (Exception e)
            {
                log(e);
            }

            return GoTo;
        }
    }
}