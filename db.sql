﻿USE [DB]
GO

/****** Object: Table [dbo].[Students] Script Date: 1/16/2019 7:16:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Students] (
    [StudentId]   INT           IDENTITY (1, 1) NOT NULL,
    [StudentName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED ([StudentId] ASC),
    CONSTRAINT [AK_Students] UNIQUE NONCLUSTERED ([StudentName] ASC)
);

GO

/****** Object: Table [dbo].[Courses] Script Date: 1/16/2019 7:16:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Courses] (
    [CourseId]   INT           IDENTITY (101, 1) NOT NULL,
    [CourseName] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([CourseId] ASC),
    CONSTRAINT [AK_Courses_CourseName] UNIQUE NONCLUSTERED ([CourseName] ASC)
);

GO

/****** Object: Table [dbo].[Professors] Script Date: 1/16/2019 7:16:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Professors] (
    [Index]         INT           IDENTITY (1, 1) NOT NULL,
    [ProfessorName] NVARCHAR (50) NOT NULL,
    [CourseId]      INT           NOT NULL,
    CONSTRAINT [PK_Professors] PRIMARY KEY CLUSTERED ([Index] ASC),
    CONSTRAINT [AK_Professors_CourseId] UNIQUE NONCLUSTERED ([CourseId] ASC),
    CONSTRAINT [FK_Professors_Courses] FOREIGN KEY ([CourseId]) REFERENCES [dbo].[Courses] ([CourseId]) ON DELETE CASCADE ON UPDATE CASCADE
);

GO

/****** Object: Table [dbo].[StudentEnrollment] Script Date: 1/16/2019 7:16:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudentEnrollment] (
    [Index]     INT IDENTITY (1, 1) NOT NULL,
    [StudentId] INT NOT NULL,
    [CourseId]  INT NOT NULL,
    CONSTRAINT [PK_StudentEnrollment] PRIMARY KEY CLUSTERED ([Index] ASC),
    CONSTRAINT [AK_StudentEnrollment_StudentId_CourseId] UNIQUE NONCLUSTERED ([CourseId] ASC, [StudentId] ASC),
    CONSTRAINT [FK_StudentEnrollment_Courses] FOREIGN KEY ([CourseId]) REFERENCES [dbo].[Courses] ([CourseId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_StudentEnrollment_Students] FOREIGN KEY ([StudentId]) REFERENCES [dbo].[Students] ([StudentId]) ON DELETE CASCADE ON UPDATE CASCADE
);









