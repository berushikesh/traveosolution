﻿using System;
using static TraveoSolution.View;
using static TraveoSolution.Controller.StudentController;

namespace TraveoSolution.ToDos
{
    public static class StudentToDo
    {
        public static char GoTo { get; set; }

        public static char Handle(int mc)
        {
            try
            {
                switch (mc)
                {
                    case 1:
                        getAllStudents();
                        GoTo = alert() == 1 ? 'A' : ' ';
                        break;
                    case 2:
                        addStudent();
                        GoTo = alert() == 1 ? 'A' : ' ';
                        break;
                    case 3:
                        updateStudent();
                        GoTo = alert() == 1 ? 'A' : ' ';
                        break;
                    case 4:
                        deleteStudent();
                        GoTo = alert() == 1 ? 'A' : ' ';
                        break;
                    default:
                        invalidEntry();
                        GoTo = alert() == 1 ? 'A' : ' ';
                        break;
                }
            }
            catch (Exception e)
            {
                log(e);
            }

            return GoTo;
        }
    }
}