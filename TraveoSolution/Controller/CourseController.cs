﻿using System;
using System.Drawing;
using System.Linq;
using static TraveoSolution.View;
using Console = Colorful.Console;

namespace TraveoSolution.Controller
{
    public static class CourseController
    {
        private static readonly DBDataContext Context = new DBDataContext();

        public static void addCourse()
        {
            divider();
            Console.Write("\t ENTER COURSE NAME: ", Color.White);
            var c = new Course
            {
                CourseName = Console.ReadLine()
            };
            Context.Courses.InsertOnSubmit(c);
            Context.SubmitChanges();
            divider();
            Console.WriteLine(" SUCCESS\n\n RECORD INSERTED ", Color.SpringGreen);
            getCourse(c.CourseName);
        }

        public static void updateCourse()
        {
            divider();

            Console.WriteLine("\n\t COURSE ID IS NEEDED. SELECT FROM FOLLOWING. ");
            getAllCourses();

            Console.Write("\t ENTER COURSE ID: ", Color.White);
            var id = Convert.ToInt32(Console.ReadLine());
            var course = Context.Courses.SingleOrDefault(e => e.CourseId == id);
            Console.Write("\t ENTER COURSE NAME: ", Color.White);
            if (course != null)
            {
                course.CourseName = Console.ReadLine();
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD UPDATED ", Color.SpringGreen);
                getCourse(course.CourseName);
            }
            else
            {
                invalidEntry();
            }
        }

        public static void getAllCourses()
        {
            var courses = Context.Courses;
            header();
            foreach (var course in courses)
                Console.WriteLine("\t COURSE ID: " + course.CourseId + " | COURSE NAME: " + course.CourseName,
                    Color.OrangeRed);

            divider();
        }

        public static void getCourse(string ctn)
        {
            var course = Context.Courses.SingleOrDefault(e => e.CourseName == ctn);
            header();

            if (course != null)
                Console.WriteLine("\t COURSE ID: " + course.CourseId + " | COURSE NAME: " + course.CourseName,
                    Color.OrangeRed);
            else
                invalidEntry();

            divider();
        }

        public static void deleteCourse()
        {
            divider();

            Console.WriteLine("\n\t COURSE ID IS NEEDED. SELECT FROM FOLLOWING. ");
            getAllCourses();

            Console.Write("\t ENTER COURSE ID: ", Color.White);
            var id = Convert.ToInt32(Console.ReadLine());
            var course = Context.Courses.SingleOrDefault(e => e.CourseId == id);
            if (course != null)
            {
                Context.Courses.DeleteOnSubmit(course);
                Context.SubmitChanges();
                divider();
                Console.WriteLine(" SUCCESS\n\n RECORD DELETED ", Color.SpringGreen);
                getAllCourses();
            }
            else
            {
                invalidEntry();
            }
        }
    }
}